<?php
namespace wa\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockHelpers::class ) ) {
	class BlockHelpers {
		public function __construct() {
		}

		static public function block_json( $name, $file = 'block.json' ) {
			return \wa\Plugin\Gutenberg\PATH . 'public/blocks/' . $name . '/' . $file;
		}

		static function gutenberg_classes( $classes, $attributes ) {
			if ( array_key_exists( 'align', $attributes ) ) {
				$classes[] = "align{$attributes['align']}";
			} else {
				$classes[] = 'alignfull';
			}

			if ( array_key_exists( 'backgroundColor', $attributes ) ) {
				$classes[] = "has-{$attributes['backgroundColor']}-background-color";
			}

			if ( array_key_exists( 'textColor', $attributes ) ) {
				$classes[] = "has-{$attributes['textColor']}-text-color";
			}

			if ( array_key_exists( 'className', $attributes ) ) {
				$classes[] = $attributes['className'];
			}

			return $classes;
		}
	}

	new BlockHelpers();
}