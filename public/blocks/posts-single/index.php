<?php
namespace wa\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( PostSingle::class ) ) {
	class PostSingle {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
			add_action( 'rest_api_init', [$this, 'register_route'] );
		}

		public static function get_author( $post_id ) {
			$name     = get_post_meta( $post_id, 'wa_author_name', true );
			$company  = get_post_meta( $post_id, 'wa_author_company', true );
			$position = get_post_meta( $post_id, 'wa_author_position', true );

			if ( $name && $company ) {
				$company = ' - ' . $company;
			}
			if ( $position && ( $name || $company ) ) {
				$position = ', ' . $position;
			}

			return $name . $company . $position;
		}

		public function get_filtered_post_types() {
			$types = get_post_types();

			$types_exclude = ['attachment', 'revision', 'nav_menu_item', 'custom_css', 'customize_changeset', 'oembed_cache', 'wp_block', 'wp_global_styles', 'user_request', 'wp_template', 'wp_template_part', 'acf-field-group', 'acf-field', 'popupbuilder', 'wp_navigation', 'page'];
			//$types_exclude = apply_filters( 'wa_post_block_types_exclude', $types_exclude );

			$types = \array_diff( $types, $types_exclude );
			//$types = apply_filters( 'wa_post_block_types', $types );

			return $types;
		}

		public function get_filtered_posts( $attributes, $show_all_posts ) {
			$order    = $attributes['order'];
			$ordering = preg_split( '#/#', $order );
			//var_dump( $ordering );

			if ( count( $ordering ) == 2 ) {
				$ordering = ['order' => $ordering[1], 'orderBy' => $ordering[0]];
			} else {
				$ordering = ['order' => 'desc', 'orderBy' => 'title'];
			}

			$args = [
				'posts_per_page' => -1,
				'post_status'    => 'publish',
				'order'          => $ordering['order'],
				'orderby'        => $ordering['orderBy'],
				'post_type'      => $show_all_posts ? $this->get_filtered_post_types() : $attributes['postType'],

				//'suppress_filters' => false
			];

			$recent_posts = get_posts( $args );

			return $recent_posts;
		}

		public function get_image_info( $thumbnail_id ) {
			$thumbnail = wp_get_attachment_image_src( $thumbnail_id, 'medium' );

			$image = [
				'srcset'  => '',
				'src'     => '',
				'width'   => '',
				'height'  => '',
				'caption' => '',
			];
			if ( $thumbnail ) {
				$image = [
					'srcset'  => wp_get_attachment_image_srcset( $thumbnail_id ),
					'src'     => $thumbnail[0],
					'width'   => $thumbnail[1],
					'height'  => $thumbnail[2],
					'caption' => wp_get_attachment_caption( $thumbnail_id ),
				];
			}

			return $image;
		}

		// Get info with meta
		public function get_posts_by_type( \WP_REST_Request $request ) {
			$attributes = $request->get_params();
			$info       = $this->get_posts_info( $attributes, true );

			if ( $info ) {
				return new \WP_REST_Response(
					[
						'success'  => true,
						'response' => $info,
					],
					200
				);
			} else {
				return new \WP_REST_Response(
					[
						'error'      => true,
						'success'    => false,
						'error_code' => 'no_posts',
						'response'   => __( 'No ' . $request['type'] . ' posts were found.', 'wa-theme' ),
					],
					401
				);
			}
		}

		public function get_posts_info( $attributes, $show_all_posts = false ) {
			$posts = $this->get_filtered_posts( $attributes, $show_all_posts );

			$return = [];
			if ( $show_all_posts ) {
				$return = [];
			} else {
				$return = [
					'label'       => '',
					'archive'     => '',
					'posts'       => [],
					//'blocks'      => [],
					'compilation' => '',
					'categories'  => [],
				];
			}

			$categories = [];
			foreach ( $posts as $post ) {
				$id               = $post->ID;
				$type             = $post->post_type;
				$post_type_object = get_post_type_object( $type );

				$categories[$type]['all'] = ['label' => 'All', 'value' => 'all'];
				$category                 = get_the_category( $id );
				$caregory_slug            = '';
				if ( is_array( $category ) && ! empty( $category ) ) {
					$caregory_slug                     = $category[0]->slug;
					$categories[$type][$caregory_slug] = ['label' => $category[0]->name, 'value' => $caregory_slug];
				}

				$label_name = $post_type_object->labels->name;

				$date      = false;
				$date_text = get_the_date( '', $id );
				if ( array_key_exists( 'displayDate', $attributes ) && $attributes['displayDate'] ) {
					$date = $date_text;
				}

				$author      = false;
				$author_text = get_the_author_meta( 'display_name', get_post_field( 'post_author', $id ) );
				if ( array_key_exists( 'displayAuthor', $attributes ) && $attributes['displayAuthor'] ) {
					$author = $author_text;
				}

				$link = get_permalink( $id );

				$archive = false;
				if ( array_key_exists( 'displaySectionMoreButton', $attributes ) && $attributes['displaySectionMoreButton'] ) {
					$archive = $this->get_read_more( $type );
				}

				//$categories = get_the_category( $id );

				$image_link = false;
				if ( has_post_thumbnail( $post ) ) {
					$thumbnail_id = get_post_thumbnail_id( $id );
					$image_info   = $this->get_image_info( $thumbnail_id );
					$image_link   = $image_info['src'];
				}

				$title_text = get_the_title( $id );

				if ( post_password_required( $post ) ) {
					$post_content_text = __( 'This content is password protected.' );
					$post_excerpt_text = __( 'This content is password protected.' );
				} else {
					$post_content_text = wp_strip_all_tags( get_the_content( null, false, $id ) );
					$post_excerpt_text = wp_strip_all_tags( get_the_excerpt( $id ) );

					$content = '';
					if ( array_key_exists( 'displayContent', $attributes ) && 'content' === $attributes['displayContent'] && $post_content_text ) {
						$content = $post_content_text;
					} else if ( array_key_exists( 'displayContent', $attributes ) && 'excerpt' === $attributes['displayContent'] && $post_excerpt_text ) {
						$content = $post_excerpt_text;
					} else if ( array_key_exists( 'displayContent', $attributes ) && $post_excerpt_text ) {
						$content = $post_excerpt_text;
					} else if ( array_key_exists( 'displayContent', $attributes ) && $post_content_text ) {
						$content = $post_content_text;
					}

					if ( array_key_exists( 'limitLength', $attributes ) && $attributes['limitLength'] ) {
						$content = wp_trim_words( $content, $attributes['textLength'] );
					}
				}

				$content_tag       = false;
				$post_content_text = $post_content_text;
				$post_excerpt_text = $post_excerpt_text;
				if ( array_key_exists( 'displayContent', $attributes ) && $attributes['displayContent'] && 'none' != $attributes['displayContent'] ) {
					$content_tag = $content;
					//var_dump( $id . ' ' . $content );
				}

				$post = [
					'id'           => $id,
					'link'         => $link,
					'image'        => $image_link,
					'title'        => $title_text,
					'author'       => $author_text,
					'date'         => $date_text,
					'content'      => $content,
					'post_content' => $post_content_text,
					'post_excerpt' => $post_excerpt_text,
					'category'     => $caregory_slug,
					//'extra'        => $extra,
					//'block'        => '<div class="block post-' . $id . ' col-12 col-lg-3 col-md-6 col-sm-12">' . $image . $content . $extra . '</div>',
				];

				$classes = ['block post-' . $id . ' col-12'];

				$classes = implode( ' ', $classes );

				$current = [
					'label'   => $label_name,
					'archive' => $archive,
					'classes' => $classes,
				];

				if ( $show_all_posts ) {
					$return['by_type'][$type]['label']   = $label_name;
					$return['by_type'][$type]['archive'] = $archive;
					$return['by_type'][$type]['posts'][] = $post;
					$return['by_type'][$type]['classes'] = $classes;
					//$return['by_type'][$type]['blocks'][] = $post['block'];
				} else {
					$return['label']   = $label_name;
					$return['archive'] = $archive;
					$return['posts'][] = $post;
					$return['classes'] = $classes;
					//$return['blocks'][] = $post['block'];
				}
			}

			if ( $show_all_posts ) {
				foreach ( $return['by_type'] as $key => $info ) {
					$post_type_object  = get_post_type_object( $key );
					$return['types'][] = [
						'label' => $post_type_object->labels->name,
						'value' => $key,
					];
					$return['categories'][$key] = $categories[$key];

					//$return['by_type'][$key]['compilation'] = $return['by_type'][$key]['label'] . '<div class="posts row">' . implode( '', $return['by_type'][$key]['blocks'] ) . '</div>' . $return['by_type'][$key]['archive'];
				}
			} else {
				//$return['compilation'] = $return['label'] . '<div class="posts row">' . implode( '', $return['blocks'] ) . '</div>' . $return['archive'];
			}

			foreach ( $return['categories'] as $key => $info ) {
				$return['categories'][$key] = array_values( $info );
			}

			return $return;
		}

		public function get_read_more( $type ) {
			$blog_url = get_permalink( get_option( 'page_for_posts' ) );
			$url      = get_post_type_archive_link( $type );

			if ( 'post' === $type && $blog_url ) {
				// Blog posts need custom links
				$url = $blog_url;
			}

			return $url;
		}

		public function manage_options_permission() {
			if ( ! current_user_can( 'manage_options' ) ) {
				return new \WP_REST_Response(
					[
						'message'  => __( 'User doesn\'t have permissions to change options.', 'wa-theme' ),
						'type'     => 'error',
						'code'     => 'user_dont_have_permission',
						'returned' => false,
					],
					401
				);
			}

			return true;
		}

		public function register_block() {
			$block_json_file = BlockHelpers::block_json( 'posts-single' );

			register_block_type( $block_json_file, [
				'render_callback' => [$this, 'render_callback'],
			] );
		}

		public function register_route() {
			register_rest_route(
				'wagt/v1',
				'/single_post_by_type?(?P<attributes>[\w]+)',
				[
					'methods'             => \WP_REST_Server::READABLE,
					'callback'            => [$this, 'get_posts_by_type'],
					'permission_callback' => [$this, 'manage_options_permission'],
				]
			);
		}

		public function render_callback( $attributes, $content ) {
			$info = $this->get_posts_info( $attributes );

			$label = '';
			if ( $attributes['displaySectionHeading'] ) {
				$label = '<h2 class="label">' . $info['label'] . '</h2>';
			}

			$archive = '';

			//var_dump( $info );

			//var_dump( array_keys( $info ) );
			if ( $attributes['displaySectionMoreButton'] && $info['archive'] ) {
				$archive = '<div class="archive wa-buttons"><a class="btn btn-outline-primary" href="' . $info['archive'] . '">View all</a></div>';
			}

			//var_dump( $info['posts'] );

			$item = $info['posts'][0];

			$id = $item['id'];
			//var_dump( get_the_content( null, false, $id ) );
			$meta_classes = ['meta'];

			$linked = true === $attributes['displayLink'];

			$image = '';
			if ( $attributes['displayImage'] ) {
				$thumbnail_id = get_post_thumbnail_id( $id );
				$image_info   = $this->get_image_info( $thumbnail_id );

				$image = '<img class="attachment-full size-full wp-post-image" width="' . $image_info['width'] . '" height="' . $image_info['height'] . '" src="' . $image_info['src'] . '" alt="" src-set="' . $image_info['srcset'] . '" sizes="(max-width: 2048px) 100vw, 2048px" />';

				if ( $linked ) {
					$image = '<a class="btn btn-link block-link" href=' . $item['link'] . '>' . $image . '</a>';
				}

				$image = '<div class="image">' . $image . '</div>';
			}

			$date = '';
			if ( array_key_exists( 'displayDate', $attributes ) && $attributes['displayDate'] ) {
				$date           = '<span class="date">' . $item['date'] . '</span>';
				$meta_classes[] = 'has-date';
			}

			$author = '';
			if ( array_key_exists( 'displayAuthor', $attributes ) && $attributes['displayAuthor'] ) {
				$author         = '<span class="author">' . $item['author'] . '</span>';
				$meta_classes[] = 'has-author';
			}

			$title = '<h4 class="title">' . $item['title'] . '</h4>';

			if ( $linked ) {
				$title = '<a class="btn btn-link block-link" href=' . $item['link'] . '>' . $title . '</a>';
			}

			$content_tag = '<div class="text">' . apply_filters( 'the_content', $item['content'] ) . '</div>';

			$content = $content_tag;

			$more = '';
			if ( $attributes['displayPostMoreButton'] ) {
				$more = '<a class="btn btn-outline-primary read-more" href="' . $item['link'] . '">Read More</a>';
			}

			$return = '<div class="wp-block-wa-posts-single ' . $info['classes'] . '">' . $image . '<div class="' . implode( ' ', $meta_classes ) . '">' . $author . $date . '</div>' . $title . '<div class="content">' . $content . '</div>' . $more . '</div>';

			//var_dump( $return );
			return $label . $return . $archive;
		}
	}

	new PostSingle();
}